---
title: "About Minipops"
date: 2020-12-17T22:07:03+01:00
draft: false
---

I'm [Gabe](https://noiseandnumbers.com), and I started Minipops as a way to learn more about electronic music and teach others along the way. 

Growing up, I loved listening to all kinds of electronic music. And I still do! I was drawn to the way my favorite musicians made entire worlds out of sounds they invented. 

You might think you need a fancy computer, big synthesizers, or expensive microphones to make electronic music. Fortunately, you don't. Many of the unique sounds you hear come from very low-tech tools and tricks. 

With that in mind, we have a few goals here at Minipops. They're meant to keep us true to our learners of all ages and abilities. Here they are: 

- Learning should be free and open. Money shouldn't stand between people and what they want to learn. 
- Tools should be accessible. The tools we feature -- computer programs and physical instruments -- should be low-cost, widely available, and usable. 
- Everyone should be able to see themself doing the thing they love. Music is for everybody, regardless of age, ability, home town or skin color. There's amazing diversity in the history of electronic music, and that diversity should continue! 

If you have any feedback for us, [reach out](). In the meantime, explore the site, and let's make some music! 
