---
heading: "Welcome to Minipops"
subheading: "A place to learn about electronic music."
---

This is where you can find our lessons that progress from the basics to more advanced topics. 
