---
title: "1.1: What Is Electronic Sound?"
tags: ["Lessons"]
draft: false
---

So, let's talk about two different ways to make sound: **electromechanical** and **electronic**. 

*Whew*, you might be saying. We're five paragraphs in and already throwing long, technical words around. *Electromechanical.* Yeesh. 

We defined electronic music as being made from sounds produced by electronic circuits. Our definition does not, however, include playing an electric guitar or singing into a microphone. Why not? 

These sounds are electromechanical. This means that the sound is produced by a physical action, like strumming strings or hitting a drum. Then the sound is amplified or reproduced by an electronic device. When a guitarist plays a chord on an electric guitar, the vibration of the string is detected by a device called a pickup, positioned under the strings. This device turns the *mechanical* movement of the strings into an electronic signal.

Let's contrast electromechanical instruments with electronic ones. When you press the keyboard of a synthesizer, there is no mechanical action that gets transformed directly into sound. Rather, the circuitry inside the synthesizer registers a key press, and that causes the instrument's circuits to produce the signal. They keystroke can be replaced with anything! It could be flipping a switch, or activating a motion sensor. The action itself doesn't matter. As long as its electronic circuit is complete, we'll hear the same sound from the synthesizer. 

