
 ---
title: "1.2 A Quick Look at Some Electronic Instruments"
tags: ["Lessons"]
draft: false
---

Now that we've talked a little bit about making sounds in electronic music, let's take a look at some of the interesting tools we could use to make our own songs. 

#### The Theremin 
The theremin is an instrument you can play without even touching it! It has two antennae sticking out from a small box. By moving your hands toward and away from the antennae, 
you can change the pitch and loudness of the sound the theremin produces.


#### Synthesizers 
Synthesizers are, more or less, a family of instruments. They use complex electronics that help musicians "sculpt" sounds by combining or changing sound waves. Some have keyboards or other ways to play that look like traditional instruments, but many have only dozens of buttons, wires and knobs. We'll talk more about these later. 

#### Computers 
And, of course, we can use computers to make electronic music. There are all kinds of ways to do this, though: software for manipulating sounds, tools for transforming data into music, and there are even programming languages that turn code into songs. 

___ 
We'll leave you with a question to think about. Remember, there aren't any *wrong* answers here. 

A tape player makes sound when the tape moves through it. And if you move the tape faster or slower, the pitch of the sound changes, too. Many musicians have used tape players to make cool sounds in their music. So, **would you say tape players are electronic or electromechanical instruments?**



