---
title: "1.0: What is electronic music?"
tags: ["Lessons"]
draft: false
---

Before we starting learning ***about*** electronic music, we need to answer a very important question: What ***is*** electronic music? If you've come here to learn about electronic music, you probably know a little about it. Maybe you enjoy a certain style---like the dance-ready beats of techno or the soothing sounds of ambient. We could spend all day discussing the different types, or genres, or electronic music, but that wouldn't get us closer to answering our question.

Let's use a very broad definition of electronic music. When we talk about electronic music, we mean music that uses sounds produced by electronic circuits. But that still leaves a lot of room for discussion! What about the electric guitar, which uses electronic circuitry to amplify strings? Can we only make electronic music from computers? 

In the following sections, we'll take a look at our definition and some important things to consider. Along the way, we should keep in mind that we're not the kings and queens of electronic music. A lot of very smart people have thought of, written about and tried out all manner of methods and tools for making electronic music. They might disagree with some of the opinions here, and that's ok! 

Ultimately, music isn't about being *right*. It's about playing. 




